# ubuntudocs

## Speed up system

[source](https://askubuntu.com/questions/1060118/how-to-free-up-the-memory-in-the-best-way)

```bash
# To create 4GB of swapfile,
sudo dd if=/dev/zero of=swapfile bs=1K count=4M
sudo mkswap swapfile
sudo swapon swapfile

# To disable a service
sudo systemctl stop apport
sudo systemctl mask apport
systemctl status apport

# To disable a service on startup
sudo systemctl stop apport
sudo systemctl disable apport
systemctl status apport

# watch realtime ram usage
watch -n 1 free -m
watch -n 1 cat /proc/meminfo

# free up memory either used or cached (page cache, inodes, and dentries)
sudo sync && echo 3 | sudo tee /proc/sys/vm/drop_caches***

# I use this terminal commands to see the whole startup list
cd /etc/xdg/autostart
sudo sed --in-place 's/NoDisplay=true/NoDisplay=false/g' *.desktop

```

Clean RAM memory

```bash

sudo su

# o empty the page cache, enter the following command,
echo 1 > /proc/sys/vm/drop_caches
echo 2 > /proc/sys/vm/drop_caches
echo 3 > /proc/sys/vm/drop_caches

# To empty the memory buffer and cache, execute the following command,
free && sync && echo 3 > /proc/sys/vm/drop_caches && free


```

### System tools

* [BleachBit](https://www.bleachbit.org/)
* System > Session & Startup
* [Multiload-ng](https://udda.github.io/multiload-ng/)


